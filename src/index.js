import * as canvas from './modules/canvas.js';
import * as lines from "./modules/lines.js";
import {asImage, random, range, unique} from "./modules/helpers.js";

// these are the variables you can use as inputs to your algorithms
// console.log(fxhash)   // the 64 chars hex number fed to your algorithm
// console.log(fxrand()) // deterministic PRNG function, use it instead of Math.random()

// note about the fxrand() function
// when the "fxhash" is always the same, it will generate the same sequence of
// pseudo random numbers, always

//----------------------
// defining features
//----------------------
// You can define some token features by populating the $fxhashFeatures property
// of the window object.
// More about it in the guide, section features:
// [https://fxhash.xyz/articles/guide-mint-generative-token#features]
//
// window.$fxhashFeatures = {
//   "Background": "Black",
//   "Number of lines": 10,
//   "Inverted": true
// }

const ctx = canvas.createCanvas(500, 500);

const distance = random(20, 40);

const x = [], y = [];
range(random(0, 4),random(5, 10)).forEach((_x, i) => {
    return x.push(random(_x, ctx.canvas.width - 10));
});

x.sort((a, b) => a > b ? 1 : -1)

x.every((value, index, array) => {
    if(index + 1 >= array.length) return x.sort((a, b) => a > b ? 1 : -1);
    if(array[index + 1] - value < 0 || array[index + 1] - value < distance) {
        array[index + 1] += distance;
    }
    return x.sort((a, b) => a > b ? 1 : -1);
});
x.filter(unique);


range(1,x.length).forEach((_y, i) => {
    return y.push(random(_y, ctx.canvas.height - 10));
});
y.sort((a, b) => a > b ? 1 : -1)
y.every((value, index, array) => {
    if(index + 1 >= array.length) return y.sort((a, b) => a > b ? 1 : -1);
    if(array[index + 1] - value < 0 || array[index + 1] - value < distance) {
        array[index + 1] += distance;
    }
    return y.sort((a, b) => a > b ? 1 : -1);
});
y.filter(unique);


for(let i = 0; i < x.length; i++) {
     lines.verticalLines(ctx, x[i], ctx.canvas.height);
}
for(let i = 0; i < y.length; i++) {
    lines.horizontalLines(ctx, y[i], ctx.canvas.width);
}

//lines.getIntersections();
lines.coloredRects(ctx);
const image = asImage("white", canvas, ctx);
