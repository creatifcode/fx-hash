const ratio = window.devicePixelRatio;

export function createCanvas(w, h) {
    let canvas = document.createElement("canvas");
    canvas.width = w * ratio;
    canvas.height = h * ratio;
    canvas.style.width = w + "px";
    canvas.style.height = h + "px";
    canvas.style.border = "5px solid #000";

    canvas.getContext("2d").scale(ratio, ratio);

    document.body.append(canvas);

    return canvas.getContext("2d");
}
