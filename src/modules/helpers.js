export function random(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(fxrand() * (max - min + 1)) + min;
}

export function range(from = 0, to) {
    let range = {
        from: from,
        to: to
    };

    range[Symbol.iterator] = function () {
        return {
            last: this.to, current: this.from,
            next() {
                if (this.current <= this.last) {
                    return {done: false, value: this.current++}
                } else {
                    return {done: true}
                }
            }
        }
    }
    return Array.from(range);
}

export function unique(value, index, self) {
    return self.indexOf(value) === index;
}

export function asImage(backgroundColor, canvas, context) {
    console.log(context);
    let w = context.canvas.width;
    let h = context.canvas.height;
    const data = context.getImageData(0, 0, w, h);

    let compositeOperation = context.globalCompositeOperation;

    context.globalCompositeOperation = "destination-over";

    context.fillStyle = backgroundColor;
    context.fillRect(0, 0, w, h);

    let imageData = context.canvas.toDataURL("image/jpeg");
    context.clearRect(0, 0, w, h);
    context.putImageData(data, 0, 0);
    context.globalCompositeOperation = compositeOperation;

    return imageData;
}
