import {random} from "./helpers";

let xLines = [];
let yLines = [];
let lineIntersection = [];
const colors = [
    "#ff0000",
    "#00ff00",
    "#0000ff",
    "#ffff00"
];

export function verticalLines(ctx, start, height) {
    xLines.push({x: start, y: height});
    ctx.moveTo(start, 0);
    ctx.lineTo(start, height);
    ctx.stroke();
}

export function horizontalLines(ctx, start, width) {
    yLines.push({x: width, y: start});

    ctx.moveTo(0, start);
    ctx.lineTo(width, start);
    ctx.stroke();
}
function getIntersections() {
    for(let i = 0; i < xLines.length; i++) {
        if(i+1 == xLines.length) {
            lineIntersection.push({x: xLines[i].x, y: 0, width: xLines[i].y - xLines[i].x, height: 0});
        } else {
            lineIntersection.push({x: xLines[i].x, y: 0, width: xLines[i + 1].x - xLines[i].x, height: 0});
        }
    }

    for(let i = 0; i < yLines.length; i++) {
        if(i+1 == yLines.length) {
            lineIntersection[i].y = yLines[i].y;
            lineIntersection[i].height = yLines[i].x - yLines[i].y;
        } else {
            lineIntersection[i].y = yLines[i].y;
            lineIntersection[i].height = yLines[i + 1].y - yLines[i].y;
        }
    }

    console.log(lineIntersection);
}

export function coloredRects(ctx) {
    getIntersections();
    const rectNum = random(lineIntersection.length / 2, lineIntersection.length);
    console.log(rectNum);

    for( let i = 0; i <= rectNum; i++) {
        const rect = lineIntersection[random(0, lineIntersection.length-1)];
        console.log(rect);
        ctx.fillStyle = colors[random(0, 3)];
        ctx.fillRect(rect.x, rect.y, rect.width, rect.height);
    }

}
